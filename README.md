![spexp_logo2.png](https://bitbucket.org/repo/kAjkzX/images/2384150508-spexp_logo2.png)


# **Download the the last version of Splicing Express:** #
https://bitbucket.org/jekroll/splicingexpress/get/master.zip

## **ABSTRACT** ##

**Motivation:** Alternative splicing events (ASEs) are prevalent in the transcriptome of eukaryotic species and are known to influence many biological phenomena. The identification and quantification of these events are crucial for better understanding of biological processes. The new generation of DNA sequencing technologies have allowed deep characterization of transcriptomes and made it possible to address these issues. ASEs analysis, however, represents a challenging task especially when many different samples need to be compared.  Some popular tools for the analysis of ASEs are known to report thousands of events without annotations and/or graphical representations . We developed here a new tool for the identification and visualization of ASEs that allows researchers to get new biological insights from data.

**Results:** A software suite named Splicing Express was created to perform ASEs analysis from transcriptome sequencing data. Its major goal is to serve the needs of biomedical researchers who do not have bioinformatics skills. Splicing Express performs automatic annotations of transcriptome data using gene coordinates available from the UCSC genome browser portal, allowing the analysis of data from all available species. The identification of ASEs is done by a known algorithm previously implemented in another tool named Splooce. As a final result, Splicing Express creates a set of HTML files composed of graphics and tables designed to describe the expression profile of ASEs among all analyzed samples. By using data from the Illumina Human Body Map, we show that Splicing Express is able to perform all tasks in a straightforward way, identifying well-known specific events for different tissues (results are available at http://www.bioinformatics-brazil.org/ASE_HBM).

**Availability and implementation:** Splicing Expresses is written in Perl and is suitable to run only in UNIX-like systems.


### To run Splicing Express: ###

```
> Download the last version of Splicing Express:
  https://bitbucket.org/jekroll/splicingexpress/get/master.zip
> Decompress it in your folder
> Edit "config.cnf" (there are instructions inside the file)
> Run "./runall.pl"
```

Splicing Express depends on some libraries, which can be installed through "cpan"
```
> sudo cpan -i SVG
> sudo cpan -i Statistics::Distributions
```


### GTF sample files (hg19) from the Human Body Map can be downloaded from: ###
http://www.bioinformatics-brazil.org/splicingexpress/SAMPLES_HBM


## **config.cnf** ##
Example for an analysis using the provided Human Body Map Data


```
############################################
# ASExpress Suite
# CONFIG.CNF
############################################

###################################################
# Set a reference genome using 'specie_assembly'
# (the same used for the transcriptome assembly):
#
# Example:
#> specie_assembly  hg19
#
# For listing all available species and their
# respective genome assembly IDs, run the following
# script:
#> ./scripts/get_annotation.pl list
###################################################

specie_assembly   hg19

###################################################
# Set a folder to write the results by using 'results_dir'
#
# Example:
#> results_dir  ./HTML_RESULTS
###################################################

results_dir       ./HTML_RESULTS

###################################################
# Set a name for the analysis using 'analysis_name'
# The name must be between quotes!
#
# Example:
#> analysis_name  "Brain: Normal vs Tumor"
###################################################

analysis_name     "Human Body Map"

###################################################
# Define the samples to be analyzed:
#> input    <gtf file path>   <Sample name>   <Sample group>
# Attention! Spaces are not allowed for those fields
# The fields <Sample name> and <Sample group> can be omitted
#
# Example:
#> input ../GTFS/sampleA.gtf  Brain_Normal  Rep1
#> input ../GTFS/sampleB.gtf  Brain_Normal  Rep2
#> input ../GTFS/sampleE.gtf  Brain_Tumor   Rep1
#
###################################################

input ../HBM_adipose.gtf adipose
input ../HBM_adrenal.gtf adrenal
input ../HBM_brain.gtf brain
input ../HBM_breast.gtf breast
input ../HBM_colon.gtf colon
input ../HBM_heart.gtf heart
input ../HBM_kidney.gtf kidney
input ../HBM_leukocyte.gtf leukocyte
input ../HBM_liver.gtf liver
input ../HBM_lung.gtf lung
input ../HBM_lymphnode.gtf lymphnode
input ../HBM_ovary.gtf ovary
input ../HBM_prostate.gtf prostate
input ../HBM_skeletalmuscle.gtf skeletalmuscle
input ../HBM_testis.gtf testis
input ../HBM_thyroid.gtf thyroid
```