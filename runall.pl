#!/usr/bin/perl -w

#The MIT License (MIT)
#
#Copyright (c) 2015 José Eduardo Kroll
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

use strict;
$| = 1;


### check library sub
sub try_load {
  my $mod = shift;

  eval("use $mod");

  if ($@) {
    return(0);
  } else {
    return(1);
  }
}


###################################
my $html_dir = '';
my $analysis_name = '';
my $assem_id = '';
mkdir "tmp";
mkdir "ref_data";

###
if (try_load('SVG')) {
  print "Checking library SVG...Ok\n";
} else {
  print "ERROR: Please install Perl SVG library using cpan!\n";
  print "Try >      sudo cpan -i SVG\n\n";
  exit;
}

if (try_load('Statistics::Distributions')) {
  print "Checking library Statistics::Distributions...Ok\n";
} else {
  print "ERROR: Please install Perl Statistics::Distributions library using cpan!\n\n";
  print "Try >      sudo cpan -i Statistics::Distributions\n\n";
  exit;
}

###
print "Removing old files 1/2...";
`rm -f ./tmp/* 2> /dev/null`;
print "ok\n";

### convert gtf to psl
my $config_filename = 'config.cnf';
unless ( -e $config_filename ) {
  die "Could not find config.cnf";
}

#load config
foreach my $sample_info ( `cat $config_filename` ) {
  chomp $sample_info;
  next if ( !$sample_info || $sample_info =~ /^\s*#/ );

  #Check reference data
  if ( $sample_info =~ /^\s*specie_assembly\s+(\S+)/i ) {
    $assem_id = $1;
    unlink 'refFlat.txt.gz';
    unlink 'refSeqAli.txt.gz';

    #no ref found
    unless (-e "./ref_data/$assem_id\_Flat.txt.gz" || -e "./ref_data/$assem_id\_Ali.txt.gz") {
      print "Downloading reference data for genome $assem_id...";

      #download ref data
      my $errors = `./scripts/get_annotation.pl $assem_id 2> /dev/null`;
      if ( $errors ) {
        print "\n$errors\n";
        print "Run\n\t./scripts/get_annotation.pl list\n\nto see all available species and their respective genome assemblies.\n";
        die "Please correct it and run ASexpress again.\n\n";
      }

      #get real assem name
      $assem_id = `cat ./tmp/assem.txt`;
      chomp $assem_id;

      #move data to final folder
      `mv refFlat.txt.gz ./ref_data/$assem_id\_Flat.txt.gz 2> /dev/null`;
      `mv refSeqAli.txt.gz ./ref_data/$assem_id\_Ali.txt.gz 2> /dev/null`;

      #data downloaded?
      unless ( -e "./ref_data/$assem_id\_Flat.txt.gz" || -e "./ref_data/$assem_id\_Ali.txt.gz" ) {
        die "\nCould not download reference data, please check the 'genome assembly name' and/or your internet connection...";
      }

      print "ok\n";

    } else {

      print "Reference data for genome $assem_id already exists...ok\n";
      print "\n\t#if you need to use an updated reference data, remove the files\n";
      print "\t#./ref_data/$assem_id\_Flat.txt.gz and ./ref_data/$assem_id\_Ali.txt.gz and run ASExpress again\n\n";
    }

    next;
  } elsif ( $sample_info =~ /^\s*results_dir\s+(\S+)/i ) {
    $html_dir = $1;
    $html_dir =~ s/\/+$//i;

    #print "Removing old files - part2...";
    print "Removing old files 2/2...";
    `rm -fR $html_dir 2> /dev/null`;
    print "ok\n";

    #check if folder was succesfuly created
    mkdir $html_dir;
    `echo check > $html_dir/check.txt 2> /dev/null`;
    unless (`cat $html_dir/check.txt 2> /dev/null`) {
      die "Could not create folder. Check it and try again.";
    }
    print "Checking results folder...ok\n";

  } elsif ( $sample_info =~ /^\s*analysis_name\s+"(.*?)"/i ) {
    $analysis_name = $1;

  } elsif ( $sample_info =~ /^\s*input/i ) {
    #Check GTF files
    my ( $null, $filename, $tag, $group ) = ( '', '', '', '' );
    ( $null, $filename, $tag, $group ) = split /\s+/, $sample_info;

    #no sample name defined
    unless ( $tag ) {
      ( $tag ) = $filename =~ /([^\/]+)$/i;
      $tag =~ s/\..*?$//i;
    }

    #no group defined
    unless ($group) {
      $group = $tag;
    }

    my $old_tag = $tag;
    $tag .= "%$group";

    #gtf doesnt exist
    unless (-e $filename) {
      die "Could not find $filename";
    }

    #error defining name for sample
    unless ($tag) {
      die "No 'sample name' found for $filename";
    }

    if ($tag =~ /!/) {
      die "'!' invalid character for tag";
    }

    #convert gtf to workable format
    print "Reading $old_tag...";
    print `./scripts/convert_to_psl.pl $filename $tag 2> ./tmp/psl_err.txt > ./tmp/gtf_$tag\.psl`;
    print "ok\n";

    #ops, errors!
    my $errors = `cat ./tmp/psl_err.txt 2> /dev/null`;
    if ( $errors ) {
      print "\n$errors\n";
      print "Errors were find in your file. Please correct it and run ASexpress again.\n\n";
      die;
    }
  }
}

#No assembly declared
unless ( $assem_id ) {
  print "No 'specie_assembly' defined in config.cnf\n";
  print "Please correct it and run ASexpress again.\n\n";
  die;
}

### Clean tmp
unless ($html_dir) {
  print "\nNo folder was provided for results. Saving results to ./RESULTS/\n\n";
  $html_dir = './RESULTS/';

  #print "Removing old files - part2...";
  print "Removing old files 2/2...";
  `rm -fR $html_dir 2> /dev/null`;
  print "ok\n";

  #check if folder was succesfuly created
  mkdir $html_dir;
  `echo check > $html_dir/check.txt 2> /dev/null`;
  unless (`cat $html_dir/check.txt 2> /dev/null`) {
    die "Could not create folder. Check it and try again.";
  }
  print "Checking results folder...ok\n";
}

### no name defined for analysis 
unless ($analysis_name) {
  $analysis_name = 'unknown';
}

### List chr names
my @CHRS = `awk '{print \$3}' ./tmp/gtf_*.psl 2> /dev/null | sort -u `;

### Identify AS Events
print "Processing AS events...";
foreach my $chro ( @CHRS ) {
  chomp $chro;
  print `./scripts/create_maphuman.pl $chro $assem_id`;
  print `./scripts/ASEclust.pl ./tmp/mapping_$chro\.txt >> ./tmp/clustres_final.txt`;
  print ".";
}
print "ok\n";

### Create html data
print "Creating HTML files...";
print `./scripts/asdb.pl ./tmp/clustres_final.txt $assem_id $html_dir`;
print `./scripts/create_webpage.pl "$analysis_name" $assem_id $html_dir`;

### Copy final HTML files to results folder
`cp -R ./htmls/* $html_dir`;
print "ok\nAll done!\n\n";
