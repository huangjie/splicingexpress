#!/usr/bin/perl -w

#The MIT License (MIT)
#
#Copyright (c) 2015 José Eduardo Kroll
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

use strict;
use SVG;
use Statistics::Distributions;

my $assem_id = $ARGV[1];
my $out_dir = $ARGV[2];
mkdir $out_dir;

######### Interpret ASE analysis
################################
open DBEXPRESS, "<$ARGV[0]";
my $gene = '';
my $event = '';
my $clust = '';
my $as_exist = '';
my $genpos = '';
my $gene_stat = '';
my $gene_exp = '';
my $binpos = '';
my $chr = '';
my $strand = '';

my %DATA = ();
my %DATA_AS = ();
my %STDERR = ();
my %REF_STRU = ();
my %EVENTS_COUNT = ();
my $para = 0;

while ( <DBEXPRESS> ) {
  chomp;
  my $linha = $_;

  if ( $linha =~ /^(?:\s*GENE\s+(\S+)|END)$/i ) {
    my $gene_aux = '';
    $gene_aux = $1 if ( $1 );

    if ($gene) {
      my $first = uc substr( $gene, 0, 1 );
      mkdir "$out_dir/$first";
      mkdir "$out_dir/$first/$gene";

      my $check_sampexp = 0;
      map {
        $check_sampexp += $DATA{ $_ };
      } keys %DATA;

      if ( $check_sampexp ) {
        draw_graph(
          \%DATA,
          "$out_dir/$first/$gene/gene_$gene.svg"
        );
      }

      open STAT, ">$out_dir/$first/$gene/STAT.txt";
      print STAT "MEDIAN $gene_exp\n";
      
      foreach my $events ( sort keys %EVENTS_COUNT ) {
        print STAT "$events $EVENTS_COUNT{ $events }\n";
      }
      close STAT;
     
      $gene_stat = '';
      $gene_exp = 0;
      
      my %TISSUES = ();
      map {
        $TISSUES{ $_ } = 0;
      } keys %{ $DATA_AS{ A } }, keys %{ $DATA_AS{ B } };
      
      my $check_sampASexp = 0;
      map {
        my $A = $DATA_AS{ A }{ $_ } || 0;
        my $B = $DATA_AS{ B }{ $_ } || 0;
        $check_sampASexp += $A + $B;
      } keys %TISSUES;

      if ( $check_sampASexp ) {
        my $first = uc substr( $gene, 0, 1 );
        mkdir "$out_dir/$first";
        mkdir "$out_dir/$first/$gene";
        
        draw_graph_AS(
          \%DATA_AS,
          \%DATA,
          \%STDERR,
          "$event:$genpos",
          $binpos,
          $gene,
          $chr,
          $strand,
          $as_exist,
          "$out_dir/$first/$gene/CLUST_$gene\_$event\_$chr\_$genpos.svg"
        );
      }

      $para = 0;
      $chr = '';
      $strand = '';
      $binpos = '';
      %DATA = ();
      %DATA_AS = ();
      %REF_STRU = ();
      %EVENTS_COUNT = ();
      $event = '';
      $genpos = '';
      $clust = '';
      $as_exist = '';
    }

    #gene?
    if ( $gene_aux ) {
      $gene = $gene_aux;
    } else {
      exit;
    }

  } elsif ( $linha =~ /^\s*CHRO\s+(\S+)$/i ) {
    $chr = $1;
  
  } elsif ( $linha =~ /^\s*STRAND\s+(\S+)$/i ) {
    $strand = $1;
  
  } elsif ( $linha =~ /^\s*EXPRESS\s+(\S+)\s+(\S+)$/i ) {
    my ( $tissue, $express ) = ( $1, $2 );
    $DATA{ $tissue } = sprintf ( "%.3f", $express );
  
  } elsif ( $linha =~ /^\s*ALIGN_CLUST\s+(\S+)$/i ) {
    $para++;
    
    my %TISSUES = ();
    map {
      $TISSUES{ $_ } = 0
    } keys %{ $DATA_AS{ A } }, keys %{ $DATA_AS{ B } };
    
    my $check_sampASexp = 0;
    map {
      my $A = $DATA_AS{ A }{ $_ } || 0;
      my $B = $DATA_AS{ B }{ $_ } || 0;
      $check_sampASexp += $A + $B;
    } keys %TISSUES;

    if ( $check_sampASexp ) {
      my $first = uc substr( $gene, 0, 1 );
      mkdir "$out_dir/$first";
      mkdir "$out_dir/$first/$gene";
      
      draw_graph_AS(
        \%DATA_AS,
        \%DATA,
        \%STDERR,
        "$event:$genpos",
        $binpos,
        $gene,
        $chr,
        $strand,
        $as_exist,
        "$out_dir/$first/$gene/CLUST_$gene\_$event\_$chr\_$genpos.svg"
      );
    }

    #clean vars for next loop
    $binpos = '';
    $as_exist = '';
    $clust = $1;
    %DATA_AS = ();
    $event = '';
    $genpos = '';
  
  } elsif ( $linha =~ /^\s*AS_EXISTS\s+(\S+)\s+(\S+)$/i && $clust ) {
    ( $as_exist, my $suppseqs ) = ( $1, $2 );

    if ( $as_exist =~ /^(\d+)\/(\d+)$/i ) {
      my ( $one, $two ) = ( $1, $2 );
      if ( $one && $two ) {
        $as_exist = "Both variants are supported by RefSeqs";
      } elsif ( $one || $two ) {
        $as_exist = "Only one variant is supported by RefSeq";
      } else {
        $as_exist = "None variants are supported by RefSeqs";
      }
    }
  
  } elsif ( $linha =~ /^\s*AS_DESC\s+(\S+)$/i && $clust ) {
    $event = $1;
    $EVENTS_COUNT{ $event }++;
  
  } elsif ( $linha =~ /^\s*AS_GENPOS\s+(\S+)$/i && $clust ) {
    $genpos = $1;

  } elsif ( $linha =~ /^\s*AS_EXPRESS_A\s+(\S+)\s+(\S+)$/i && $clust ) {
    my ( $tissue, $express_aux ) = ( $1, $2 );
    my ( $express, $stderr ) = $express_aux =~ /(\S+)\((\S+)\)/;
    $DATA_AS{ 'A' }{ $tissue } = $express;
    $STDERR{ 'A' }{ $tissue } = $stderr;
  
  } elsif ( $linha =~ /^\s*AS_EXPRESS_B\s+(\S+)\s+(\S+)$/i && $clust ) {
    my ( $tissue, $express_aux ) = ( $1, $2 );
    my ( $express, $stderr ) = $express_aux =~ /(\S+)\((\S+)\)/;
    $DATA_AS{ 'B' }{ $tissue } = $express;
    $STDERR{ 'B' }{ $tissue } = $stderr;

  } elsif ( $linha =~ /^\s*AS_STAT\s+(.*?)$/i && $gene ) {
    my $stat = $1;
    $gene_stat .= "$stat\n";
  }
}



###################################### DRAW GRAPH AS !!!
sub draw_graph_AS
{
  my $aux_tissue = shift;
  my $gen_exp = shift;
  my $stderr = shift;
  my %TISSUES = %{ $aux_tissue };
  my $EVENT = shift;
  my $genbin = shift;
  my $geneaux = shift;
  my $chraux = shift;
  my $strandaux = shift;
  my $as_supp = shift;
  my $output = shift;


  ####### EXPRESS GRAPH
  my $express_x = 0;
  my $express_y = 0;
  my $max_barsize = 350;

  #### calcule median
  my $median_A = 0;
  my $median_B = 0;
  my $max_express = 0;
  my $num_tissues = scalar keys %{ $TISSUES{ 'A' } };

  #init graphics!
  my $grp1 = SVG->new(
    $max_barsize * 2 + $express_x + 140,
    $num_tissues * 20 + $express_y + 230
  );
  
  my ( $AS_type, $gnm_start, $gnm_end ) = split /:/, $EVENT; ####
  return if ( !$AS_type || ( $gnm_start eq '?' || $gnm_end eq '?' ) );

  my %AS_FULLNAMES = (
    'ALT5' => "Alternative 5\' Splice Site",
    'ALT3' => "Alternative 3\' Splice Site",
    'RETENT' => "Intron Retention",
    'SKIP' => "Exon Skipping"
  );
  my $AS_type_fullname = $AS_FULLNAMES{$AS_type};


  #### inversions
  my %invertA = %{ $TISSUES{ 'A' } };
  my %invertB = %{ $TISSUES{ 'B' } };

  if ( $AS_type eq 'SKIP') {
    %{ $TISSUES{ 'A' } } = %invertB;
    %{ $TISSUES{ 'B' } } = %invertA;
  } elsif ( ( $AS_type eq 'ALT5' || $AS_type eq 'ALT3' )) {
    %{ $TISSUES{ 'A' } } = %invertB;
    %{ $TISSUES{ 'B' } } = %invertA;
  } elsif ( $AS_type eq 'RETENT') {
    %{ $TISSUES{ 'A' } } = %invertB;
    %{ $TISSUES{ 'B' } } = %invertA;
  }


  ######## LEGEND
  my $legend_x = 20;
  my $legend_y = 30;

  my $grp1_legend = $grp1->group(
    'id' => 'leg',
    'style' => {
      'font' => [ qw(Arial) ],
      'font-size' => 11,
      'fill' => 'black',
    }
  );

  $grp1->rectangle(
    x => $express_x + $legend_x,
    y => $express_y + $legend_y,
    width => 10,
    height => 10,
    style => {
      'stroke' => 'rgb(0, 0, 0)',
      'fill' => 'rgb(0, 100, 250)'
    }
  );
  
  $grp1_legend->text(
    x => $express_x + $legend_x + 15,
    y => $express_y + $legend_y + 10,
    -cdata => 'Fold difference'
  );

  $legend_y += 15;
  $grp1->rectangle(
    x => $express_x + $legend_x,
    y => $express_y + $legend_y,
    width => 10,
    height => 10,
    style => {
      'stroke' => 'rgb(0, 0, 0)',
      'fill' => 'rgb(0, 200, 0)'
    }
  );
  
  $grp1_legend->text(
    x => $express_x + $legend_x + 15,
    y => $express_y + $legend_y + 10,
    -cdata => 'Green splicing isoform'
  );

  $legend_y += 15;
  $grp1->rectangle(
    x=>$express_x + $legend_x,
    y=>$express_y + $legend_y,
    width=>10,
    height=>10,
    style=>{
      'stroke' => 'rgb(0, 0, 0)',
      'fill' => 'rgb(200, 0, 0)'
    }
  );

  $grp1_legend->text(
    x => $express_x + $legend_x + 15,
    y => $express_y + $legend_y + 10,
    -cdata=>'Red splicing isoform'
  );


  ######## DRAW ASE SCHEMES
  my $event_y = 80;
  my $event_x = 450;

  $grp1->text(
    x => $express_x + $event_x - 70,
    y => $express_y + $event_y - 30, 
    -cdata => 'Event schematic',
    style => {
      'font' => [ qw(Arial) ],
      'font-size' => 12,
      'fill' => 'black'
    }
  );


  if ( ( $AS_type eq 'ALT5' && $strandaux eq '+' ) || ( $AS_type eq 'ALT3' && $strandaux eq '-' ) ) {
    $grp1->rectangle(
      x => $express_x + $event_x,
      y => $express_y + $event_y,
      width => 70,
      height => 20,
      style => {
        'stroke' => 'rgb( 0, 0, 0 )',
        'fill' => 'rgb( 255, 50, 50 )'
      }
    );
    
    $grp1->rectangle(
      x => $express_x + $event_x + 170,
      y => $express_y + $event_y,
      width => 70,
      height => 20,
      style => {
        'stroke' => 'rgb( 0, 0, 0 )',
        'fill' => 'rgb( 255, 50, 50 )'
      }
    );
    
    $grp1->rectangle(
      x => $express_x + $event_x + 70,
      y => $express_y + $event_y,
      width => 20,
      height => 20,
      style => {
        'stroke' => 'rgb( 0, 0, 0 )',
        'fill' => 'rgb( 50, 255, 50 )'
      }
    );

    $grp1->line(
      x1 => $express_x + $event_x + 70,
      y1 => $express_y + $event_y + 20,
      x2 => $express_x + $event_x + 120,
      y2 => $express_y + $event_y + 30,
      style => {
        'stroke' => 'rgb( 200, 0, 0 )'
      }
    );

    $grp1->line(
      x1 => $express_x + $event_x + 120,
      y1 => $express_y + $event_y + 30,
      x2 => $express_x + $event_x + 170,
      y2 => $express_y + $event_y + 20,
      style => {
        'stroke' => 'rgb( 200, 0, 0 )'
      }
    );

    $grp1->line(
      x1 => $express_x + $event_x + 90,
      y1 => $express_y + $event_y,
      x2 => $express_x + $event_x + 130,
      y2 => $express_y + $event_y - 10,
      style => {
        'stroke' => 'rgb( 0, 200, 0 )'
      }
    );
    
    $grp1->line(
      x1 => $express_x + $event_x + 130,
      y1 => $express_y + $event_y - 10,
      x2 => $express_x + $event_x + 170,
      y2 => $express_y + $event_y,
      style => {
        'stroke' => 'rgb( 0, 200, 0 )'
      }
    );

    my $aux_x = $express_x + $event_x + 68;
    my $aux_y = $express_y + $event_y - 2;
    
    $grp1->text(
      x => $aux_x,
      y => $aux_y,
      -cdata => $gnm_start,
      style => {
        'font'=> [ qw(Arial) ],
        'font-size' => 10
      },
      transform => "rotate( -40, $aux_x, $aux_y )"
    );
    
    $aux_x = $express_x + $event_x + 92;
    $aux_y = $express_y + $event_y -2;
    $grp1->text(
      x => $aux_x,
      y => $aux_y,
      -cdata => $gnm_end,
      style => {
        'font' => [ qw(Arial) ],
        'font-size' => 10
      },
      transform => "rotate( -40, $aux_x, $aux_y )"
    );


    $grp1->text(
      x => $express_x + 10,
      y => $express_y + 15,
      -cdata => $AS_type_fullname,
      style => {
        'font'=> [ qw(Arial) ],
        'font-size' => 15
      }
    );

  } elsif ( ( $AS_type eq 'ALT3' && $strandaux eq '+' ) || ( $AS_type eq 'ALT5' && $strandaux eq '-' ) ) {
    
    $grp1->rectangle(
      x=>$express_x + $event_x,
      y=>$express_y + $event_y,
      width=>70,
      height=>20,
      style=>{
        'stroke' => 'rgb( 0, 0, 0 )',
        'fill' => 'rgb( 255, 50, 50 )'
      }
    );

    $grp1->rectangle(
      x => $express_x + $event_x + 170,
      y => $express_y + $event_y,
      width => 70,
      height => 20,
      style => {
        'stroke' => 'rgb( 0, 0, 0 )',
        'fill' => 'rgb( 255, 50, 50 )'
      }
    );
    
    $grp1->rectangle(
      x => $express_x + $event_x + 150,
      y => $express_y + $event_y,
      width => 20,
      height => 20,
      style => {
        'stroke' => 'rgb( 0, 0, 0 )',
        'fill' => 'rgb( 50, 255, 50 )'
      }
    );

    $grp1->line(
      x1 => $express_x + $event_x + 70,
      y1 => $express_y + $event_y + 20,
      x2 => $express_x + $event_x + 120,
      y2 => $express_y + $event_y + 30,
      style => {
        'stroke' => 'rgb( 200, 0, 0 )'
      }
    );

    $grp1->line(
      x1 => $express_x + $event_x + 120,
      y1 => $express_y + $event_y + 30,
      x2 => $express_x + $event_x + 170,
      y2 => $express_y + $event_y + 20,
      style => {
        'stroke' => 'rgb( 200, 0, 0 )'
      }
    );

    $grp1->line(
      x1 => $express_x + $event_x + 70,
      y1 => $express_y + $event_y,
      x2 => $express_x + $event_x + 110,
      y2 => $express_y + $event_y - 10,
      style =>{
        'stroke' => 'rgb( 0, 200, 0 )'
      }
    );
    
    $grp1->line(
      x1 => $express_x + $event_x + 110,
      y1 => $express_y + $event_y - 10,
      x2 => $express_x + $event_x + 150,
      y2 => $express_y + $event_y,
      style => {
        'stroke' => 'rgb( 0, 200, 0 )'
      }
    );

    my $aux_x = $express_x + $event_x + 148;
    my $aux_y = $express_y + $event_y -2;
    $grp1->text(
      x => $aux_x,
      y => $aux_y,
      -cdata => $gnm_start,
      style => {
        'font'=> [ qw(Arial) ],
        'font-size' => 10
      },
      transform => "rotate( -40, $aux_x, $aux_y )"
    );
   
    $aux_x = $express_x + $event_x + 172;
    $aux_y = $express_y + $event_y - 2;
    $grp1->text(
      x => $aux_x,
      y => $aux_y,
      -cdata => $gnm_end,
      style => {
        'font' => [ qw(Arial) ],
        'font-size' => 10
      },
      transform => "rotate( -40, $aux_x, $aux_y )"
    );

    $grp1->text(
      x => $express_x + 10,
      y => $express_y + 15,
      -cdata => $AS_type_fullname,
      style => {
        'font'=> [ qw(Arial) ],
        'font-size' => 15
      }
    );
  
  } elsif ($AS_type eq 'SKIP') {

    $grp1->rectangle(
      x => $express_x + $event_x,
      y => $express_y + $event_y,
      width => 70,
      height => 20,
      style => {
        'stroke' => 'rgb( 0, 0, 0 )',
        'fill' => 'rgb( 255, 50, 50 )'
      }
    );
    
    $grp1->rectangle(
      x => $express_x + $event_x + 170,
      y => $express_y + $event_y,
      width => 70,
      height => 20,
      style => {
        'stroke' => 'rgb(0, 0, 0)',
        'fill' => 'rgb(255, 50, 50)'
      }
    );
    
    $grp1->rectangle(
      x => $express_x + $event_x + 110,
      y => $express_y + $event_y,
      width => 20,
      height => 20,
      style => {
        'stroke' => 'rgb( 0, 0, 0 )',
        'fill' => 'rgb( 50, 255, 50 )'
      }
    );

    $grp1->line(
      x1 => $express_x + $event_x + 70,
      y1 => $express_y + $event_y + 20,
      x2 => $express_x + $event_x + 120,
      y2 => $express_y + $event_y + 30,
      style => {
        'stroke' => 'rgb(200, 0, 0)'
      }
    );

    $grp1->line(
      x1 => $express_x + $event_x + 120,
      y1 => $express_y + $event_y + 30,
      x2 => $express_x + $event_x + 170,
      y2 => $express_y + $event_y + 20,
      style => {
        'stroke' => 'rgb( 200, 0, 0 )'
      }
    );

    $grp1->line(
      x1 => $express_x + $event_x + 70,
      y1 => $express_y + $event_y,
      x2 => $express_x + $event_x + 90,
      y2 => $express_y + $event_y -10,
      style => {
        'stroke' => 'rgb( 0, 200, 0 )'
      }
    );
    
    $grp1->line(
      x1 => $express_x + $event_x + 90,
      y1 => $express_y + $event_y - 10,
      x2 => $express_x + $event_x + 110,
      y2 => $express_y + $event_y,
      style => {
        'stroke' => 'rgb( 0, 200, 0 )'
      }
    );
    
    $grp1->line(
      x1 => $express_x + $event_x + 130,
      y1 => $express_y + $event_y,
      x2 => $express_x + $event_x + 150,
      y2 => $express_y + $event_y - 10,
      style => {
        'stroke' => 'rgb( 0, 200, 0 )'
      }
    );
    
    $grp1->line(
      x1 => $express_x + $event_x + 150,
      y1 => $express_y + $event_y - 10,
      x2 => $express_x + $event_x + 170,
      y2 => $express_y + $event_y,
      style => {
        'stroke' => 'rgb( 0, 200, 0 )'
      }
    );

    my $aux_x = $express_x + $event_x + 108;
    my $aux_y = $express_y + $event_y - 2;
    $grp1->text(
      x => $aux_x,
      y => $aux_y,
      -cdata => $gnm_start,
      style => {
        'font' => [ qw(Arial) ],
        'font-size' => 10
      },
      transform => "rotate( -40, $aux_x, $aux_y )"
    );
 
    $aux_x = $express_x + $event_x + 132;
    $aux_y = $express_y + $event_y - 2;
    $grp1->text(
      x => $aux_x,
      y => $aux_y,
      -cdata => $gnm_end,
      style => {
        'font'=> [ qw(Arial) ],
        'font-size' => 10
      },
      transform => "rotate( -40, $aux_x, $aux_y )"
    );

    $grp1->text(
      x => $express_x + 10,
      y => $express_y + 15,
      -cdata => $AS_type_fullname,
      style => {
        'font'=> [ qw(Arial) ],
        'font-size' => 15
      }
    );
  
  } elsif ( $AS_type eq 'RETENT' ) {
    
    $grp1->rectangle(
      x => $express_x + $event_x,
      y => $express_y + $event_y,
      width => 70,
      height => 20,
      style => {
        'stroke' => 'rgb( 0, 0, 0 )',
        'fill' => 'rgb( 255, 50, 50 )'
      }
    );
    
    $grp1->rectangle(
      x => $express_x + $event_x + 170,
      y => $express_y + $event_y,
      width => 70,
      height => 20,
      style => {
        'stroke' => 'rgb( 0, 0, 0 )',
        'fill' => 'rgb( 255, 50, 50 )'
      }
    );
    
    $grp1->rectangle(
      x => $express_x + $event_x + 70,
      y => $express_y + $event_y,
      width => 100,
      height => 20,
      style => {
        'stroke' => 'rgb( 0, 0, 0 )',
        'fill' => 'rgb( 50, 255, 50 )'
      }
    );
 
    $grp1->line(
      x1 => $express_x + $event_x + 70,
      y1 => $express_y + $event_y + 20,
      x2 => $express_x + $event_x + 120,
      y2 => $express_y + $event_y + 30,
      style => {
        'stroke' => 'rgb( 200, 0, 0 )'
      }
    );
   
    $grp1->line(
      x1 => $express_x + $event_x + 120,
      y1 => $express_y + $event_y + 30,
      x2 => $express_x + $event_x + 170,
      y2 => $express_y + $event_y + 20,
      style => {
        'stroke' => 'rgb( 200, 0, 0 )'
      }
    );

    my $aux_x = $express_x + $event_x + 68;
    my $aux_y = $express_y + $event_y -2;
    $grp1->text(
      x => $aux_x,
      y => $aux_y,
      -cdata => $gnm_start,
      style => {
        'font'=> [ qw(Arial) ],
        'font-size' => 10
      },
      transform => "rotate( -40, $aux_x, $aux_y )"
    );
   
    $aux_x = $express_x + $event_x + 172;
    $aux_y = $express_y + $event_y -2;
    $grp1->text(
      x => $aux_x,
      y => $aux_y,
      -cdata => $gnm_end,
      style => {
        'font'=> [ qw(Arial) ],
        'font-size' => 10
      },
      transform => "rotate( -40, $aux_x, $aux_y )"
    );

    $grp1->text(
      x => $express_x + 10,
      y => $express_y + 15,
      -cdata => $AS_type_fullname,
      style => {
        'font'=> [ qw(Arial) ],
        'font-size' => 15
      }
    );
  }


  ### DRAW GRAPH BARS/AXIS...
  $express_y += 110; ## vspace

  my @EXP_MEDIAN_A = sort { $a <=> $b } values %{ $TISSUES{ A } };
  my @EXP_MEDIAN_B = sort { $a <=> $b } values %{ $TISSUES{ B } };

  ###### MEDIANS
  my $exp_median_size_A = scalar @EXP_MEDIAN_A;
  if ( $exp_median_size_A % 2 ) {
    $median_A = ( $EXP_MEDIAN_A[ $exp_median_size_A / 2 - 1 ]
      + $EXP_MEDIAN_A[ $exp_median_size_A / 2 ] ) / 2;
  } else {
    $median_A = $EXP_MEDIAN_A[ ( $exp_median_size_A + 1 ) / 2 - 1 ];
  }

  my $exp_median_size_B = scalar @EXP_MEDIAN_B;
  if ( $exp_median_size_B % 2 ) {
    $median_B = ( $EXP_MEDIAN_B[ $exp_median_size_B / 2 - 1 ]
      + $EXP_MEDIAN_B[ $exp_median_size_B / 2 ] ) / 2;
  } else {
    $median_B = $EXP_MEDIAN_B[ ( $exp_median_size_B + 1 ) / 2 - 1 ];
  }

  ### MAX
  $max_express = $EXP_MEDIAN_A[ -1 ];
  if ( $EXP_MEDIAN_B[ -1 ] > $max_express ) {
    $max_express = $EXP_MEDIAN_B[ -1 ];
  }

  #maximum value for FPKM axis
  my $max_divisor = 12;
  if ($max_express > $max_divisor) {
    $max_divisor = $max_express;
  }
  $max_divisor++ if ($max_divisor % 2);


  #### draw axis FPKM A
  $express_y += 11;
  $grp1->text(
    x => $express_x + $max_barsize * 2 + 15,
    y => $express_y + 40 - 2,
    -cdata => 'FPKM',
    style => {
      'font' => [ qw(Arial) ],
      'font-size' => 12
    }
  );

  $express_y += 30;
  $grp1->line(
    x1 => $express_x,
    y1 => $express_y,
    x2 => $express_x + $max_barsize,
    y2 => $express_y,
    style => {
      'stroke' => 'rgb(0, 0, 0)'
    }
  );

  foreach my $steps ( 0..10 ) {
    unless ( $steps % 5 ) {
      $grp1->line(
        x1 => $express_x + ( $steps * $max_barsize / 10 ),
        y1 => $express_y,
        x2 => $express_x + ( $steps * $max_barsize / 10 ),
        y2 => $express_y + 8,
        style => {
          'stroke' => 'rgb( 0, 0, 0 )'
        }
      );

      my $express = int ( $max_divisor - ( $max_divisor / 10 ) * $steps );
      my $aux_x = $express_x + ( $steps * $max_barsize / 10 ) + 2;
      my $aux_y = $express_y - 2;
      $grp1->text(
        x => $aux_x,
        y => $aux_y,
        -cdata => $express,
        style => {
          'font' => [ qw(Arial) ],
          'font-size' => 10
        },
        transform => "rotate( -25,$aux_x,$aux_y )"
      );

    } else {
      
      $grp1->line(
        x1 => $express_x + ( $steps * $max_barsize / 10 ),
        y1 => $express_y,
        x2 => $express_x + ( $steps * $max_barsize / 10 ),
        y2 => $express_y + 4,
        style => {
          'stroke' => 'rgb( 0, 0, 0 )'
        }
      );
    }
  }

  #### scale FPKM line B
  $express_x += $max_barsize;
  $grp1->line(
    x1 => $express_x,
    y1 => $express_y,
    x2 => $express_x + $max_barsize,
    y2 => $express_y,
    style => {
      'stroke' => 'rgb( 0, 0, 0 )'
    }
  );
 
  foreach my $steps ( 1..10 ) {
    unless ( $steps % 5 ) {
      $grp1->line(
        x1 => $express_x + ( $steps * $max_barsize / 10 ),
        y1 => $express_y,
        x2 => $express_x + ( $steps * $max_barsize / 10 ),
        y2 => $express_y + 8,
        style => {
          'stroke' => 'rgb( 0, 0, 0 )'
        }
      );

      my $express = int ( ( $max_divisor / 10 ) * $steps );
      my $aux_x = $express_x + ( $steps * $max_barsize / 10 ) + 2;
      my $aux_y = $express_y - 2;
      $grp1->text(
        x => $aux_x,
        y => $aux_y,
        -cdata => $express,
        style => {
          'font'=> [ qw(Arial) ],
          'font-size' => 10
        },
        transform => "rotate( -25, $aux_x, $aux_y )"
      );
    
    } else {

      $grp1->line(
        x1 => $express_x + ( $steps * $max_barsize / 10 ),
        y1 => $express_y,
        x2 => $express_x + ( $steps * $max_barsize / 10 ),
        y2 => $express_y + 4,
        style => {
          'stroke' => 'rgb( 0, 0, 0 )'
        }
      );
    }
  }



  ######## 
  my %SPECIFIC = ();
  my %SPECIFIC_SIGA = ();
  my %SPECIFIC_SIGB = ();
  my %TISSUE_PROB = ();

  ### calc fold change
  foreach my $tissue_aux ( sort keys %{ $TISSUES{ 'A' } } ) {
    my $expression_A = $TISSUES{ 'A' }{ $tissue_aux };
    my $expression_B = $TISSUES{ 'B' }{ $tissue_aux };
    
    if ( ! $expression_A && ! $expression_B ) {
      $TISSUE_PROB{ $tissue_aux } = 'Null';
    }
    
    $expression_A += 1;
    $expression_B += 1;

    my $stderr_sumA = $stderr->{ 'A' }{ $tissue_aux };
    my $stderr_sumB = $stderr->{ 'B' }{ $tissue_aux };
 
    unless ( $TISSUE_PROB{ $tissue_aux } ) { 
      my $divisor = sqrt($stderr_sumA ** 2 + $stderr_sumB ** 2);
      if ( $divisor == 0 ) {
        $TISSUE_PROB{ $tissue_aux } = 'Null';
      }
      else {
        my $tvalue = abs($expression_A - $expression_B) / $divisor;
        $TISSUE_PROB{ $tissue_aux } = Statistics::Distributions::tprob ( 1, $tvalue );
      }
    }

    $SPECIFIC{ $tissue_aux } = log ( $expression_A / $expression_B ) / log 10;
    $SPECIFIC{ $tissue_aux } = sprintf ( "%.3f", $SPECIFIC{ $tissue_aux } );

    $SPECIFIC_SIGA{ $tissue_aux } = 1 * $SPECIFIC{ $tissue_aux };
    $SPECIFIC_SIGB{ $tissue_aux } = -1 * $SPECIFIC{ $tissue_aux };
  }

  #STATISTICS REPORT #################################################################
  open FILE, ">$output.report";
  my $ucsc_url = "http://genome.ucsc.edu/cgi-bin/hgTracks?position=chr$chraux:$gnm_start-$gnm_end&db=$assem_id";
  
  print FILE "   <section id='<<ASE_COUNT>>'>
      <div class='row'>
        <div class='col-sm-6'>
          <h4><<ASE_COUNT>></h4>
          <ul class='list-unstyled'>
            <li><b>Type: </b>$AS_type_fullname</li>
            <li><b>Genomic Position: </b><a href='$ucsc_url' target='_blank'>chr$chraux:$gnm_start-$gnm_end</a><i> (link to UCSC)</i></li>
            <li><b>Support Evidence: </b>$as_supp</li>
          </ul>
          <hr>
          <img src='<<AS_GRAPH>>' class='img-responsive'>
        </div>

        <div class='col-sm-6'>
          <div class='panel panel-default'>
            <div class='panel-heading'><h5>Expression details</h5></div>
              <table class='tablesorter' id='t<<ASE_COUNT>>'>
                <thead><tr>
                  <th>Sample</th>
                  <th>Group</th>
                  <th>FPKM (both variants)</th>
                  <th>FPKM (green variant)</th>
                  <th>FPKM (red variant)</th>
                  <th>Fold difference (Log10)</th>
                  <th>p-Value</th>
              </tr></thead>
              <tbody>\n";

  my %ALL_TISSUES_AUX = ();
  map {
    $ALL_TISSUES_AUX{ $_ } = 0
  } ( sort keys %SPECIFIC_SIGA, sort keys %SPECIFIC_SIGB );
  my @ALL_TISSUES = keys %ALL_TISSUES_AUX;

  foreach my $tissue_aux ( @ALL_TISSUES ) {
    my $logfoldA = sprintf("%.3f", $SPECIFIC_SIGA{ $tissue_aux });
    my $fpkmA = sprintf("%.3f", $TISSUES{ 'A' }{ $tissue_aux });
    my $fpkmB = sprintf("%.3f", $TISSUES{ 'B' }{ $tissue_aux });
    my $pvalue = $TISSUE_PROB{ $tissue_aux };

    my $sum_fpkm = $fpkmA + $fpkmB;
    my $gene_base_fpkm = $gen_exp->{ $tissue_aux };

    if ( $logfoldA > 0 ) {
      $logfoldA = "<font color='#00CC00'>$logfoldA</font>";
    } elsif ( $logfoldA < 0 ) {
      $logfoldA = "<font color='#FF0000'>$logfoldA</font>";
    }

    if ( $pvalue ne 'Null' && $pvalue <= 0.01 ) {
      $pvalue = "<font color='#6B24B2'><b>$pvalue</b></font>";
    }
    elsif ( $pvalue ne 'Null' && $pvalue <= 0.05 ) {
      $pvalue = "<font color='#6B24B2'>$pvalue</font>";
    }

    my ( $tissue, $group ) = $tissue_aux =~ /^(.*?)%(.*?)$/;
    print FILE "              <tr><td>$tissue</td><td>$group</td><td>$sum_fpkm</td><td>$fpkmA</td><td>$fpkmB</td><td>$logfoldA</td><td>$pvalue</td></tr>\n";
  }

  print FILE "              <tbody>
            </table>
          </div>
        </div>
      </div>
    </section>
    <br><br>\n";
  close FILE;

  ######################################


  #### define order to draw - based on fold change
  my %TISSUE_ORDER_FOLD_aux = ();
  my @TISSUE_ORDER_FOLD = ();

  foreach my $tissue_aux ( keys %SPECIFIC_SIGA ) {
    my $fold = $SPECIFIC_SIGA{ $tissue_aux };
    push @{ $TISSUE_ORDER_FOLD_aux{ $fold } }, $tissue_aux;
  }

  foreach my $fold ( sort { $a <=> $b } keys %TISSUE_ORDER_FOLD_aux) {
    foreach my $tissue_aux (@{ $TISSUE_ORDER_FOLD_aux{ $fold } }) {
      push @TISSUE_ORDER_FOLD, $tissue_aux;
    }
  }

  #### rescale foldchange bar
  my $fold_scale = 0;
  my $max_fold = 0;
  foreach my $tissue_aux ( @TISSUE_ORDER_FOLD ) {
    my $expressionA = $TISSUES{ 'A' }{ $tissue_aux };
    my $expressionB = $TISSUES{ 'B' }{ $tissue_aux };
    my $fold = $SPECIFIC_SIGA{ $tissue_aux };
    
    next unless ( $fold );
    $max_fold = $fold if ( abs( $fold ) > $max_fold );

    my $fold_scale_aux = 0;
    if ( $fold < 0 ) {
      $fold_scale_aux = $expressionB / abs( $fold );
    } elsif ( $fold > 0 ) {
      $fold_scale_aux = $expressionA / $fold;
    }
    next unless ( $fold_scale_aux );
   
    #confer scale
    my $ok = 0;
    foreach my $tissue_chk ( @TISSUE_ORDER_FOLD ) {
      next if ( $tissue_chk eq $tissue_aux );
      my $expressionA_chk = $TISSUES{ 'A' }{ $tissue_chk };
      my $expressionB_chk = $TISSUES{ 'B' }{ $tissue_chk };
      my $fold_chk = $SPECIFIC_SIGA{ $tissue_chk };
      next unless ( $fold_chk );

      my $fold_bar_size = abs( $fold_chk ) * $fold_scale_aux / $max_divisor * $max_barsize;
      my $A_bar_size = $expressionA_chk / $max_divisor * $max_barsize;
      my $B_bar_size = $expressionB_chk / $max_divisor * $max_barsize;

      if ( $fold_chk < 0 && $fold_bar_size - $B_bar_size > 2 ) {
        $ok++;
      } elsif ( $fold_chk > 0 && $fold_bar_size - $A_bar_size > 2 ) {
        $ok++;
      } 
    }

    #scale accepted
    unless ($ok) {
      if ( $fold_scale_aux > $fold_scale) {
        $fold_scale = $fold_scale_aux;
      }
    }
  }


  #### scale FOLD line C
  my $fold_y = $#TISSUE_ORDER_FOLD * 20 + 55;

  $grp1->text(
    x => $express_x + $max_barsize + 15,
    y => $express_y + $fold_y,
    -cdata => 'Fdiff',
    style => {
      'font' => [ qw(Arial) ],
      'font-size' => 12
    }
  );

  $grp1->line(
    x1 => $express_x - $max_barsize,
    y1 => $express_y + $fold_y,
    x2 => $express_x + $max_barsize,
    y2 => $express_y + $fold_y,
    style => {
      'stroke' => 'rgb( 0, 0, 0 )'
    }
  );
 
  foreach my $steps ( -10 .. 10 ) {
    unless ( $steps % 5 ) {
      $grp1->line(
        x1 => $express_x + ( $steps * $max_barsize / 10 ),
        y1 => $express_y + $fold_y,
        x2 => $express_x + ( $steps * $max_barsize / 10 ),
        y2 => $express_y + $fold_y - 8,
        style => {
          'stroke' => 'rgb( 0, 0, 0 )'
        }
      );

      my $express = 0;
      if ( $steps && $fold_scale && $max_fold ) {
        $express = sprintf(
          "%.2f",
          abs( ( $max_fold / ( ( $max_fold * $fold_scale * 0.90 ) / $max_divisor ) ) / 10 * $steps)
        );
      }
      
      my $aux_x = $express_x + ( $steps * $max_barsize / 10 ) - 1;
      my $aux_y = $express_y + $fold_y + 13;
      $grp1->text(
        x => $aux_x,
        y => $aux_y, 
        -cdata => $express,
        transform => "rotate( 25, $aux_x, $aux_y )",
        style => {
          'font' => [ qw(Arial) ],
          'font-size' => 10
        }
      );
    
    } else {
    
      $grp1->line(
        x1 => $express_x + ( $steps * $max_barsize / 10 ),
        y1 => $express_y + $fold_y,
        x2 => $express_x + ( $steps * $max_barsize / 10 ),
        y2 => $express_y + $fold_y - 4,
        style => {
          'stroke' => 'rgb( 0, 0, 0 )'
        }
      );
    }
  }


  ####################################################
  #### Draw bar graph --- A
  $express_y += 20;
  my $tissue_pos_A = 0;
  foreach my $tissue_aux ( @TISSUE_ORDER_FOLD )
  {
    my $tissue = $tissue_aux;
    my $expression = $TISSUES{ 'A' }{ $tissue };
    $tissue_pos_A++;

    my $y = ( $tissue_pos_A * 20 ) - 20;
    my $x = ( $expression / $max_divisor ) * $max_barsize;
    my $color = 170 - int ( ( $expression / $max_divisor )  * 170 );

    $grp1->rectangle(
      x => $express_x - $x,
      y => $express_y + $y,
      width => $x,
      height => 15,
      style => {
        'stroke' => 'rgb( 0, 0, 0 )',
        'fill' => "rgb( $color, 255, $color )"
      }
    );
  }


  #### bar graph --- B
  my $tissue_pos_B = 0;
  foreach my $tissue_aux ( @TISSUE_ORDER_FOLD )
  {
    my $tissue = $tissue_aux;
    my $expression = $TISSUES{ 'B' }{ $tissue };
    $tissue_pos_B++;

    my $y = ( $tissue_pos_B * 20 ) - 20;
    my $x = ( $expression / $max_divisor ) * $max_barsize;
    my $color = 170 - int ( ( $expression / $max_divisor )  * 170 );

    $grp1->rectangle(
      x => $express_x,
      y => $express_y + $y,
      width => $x,
      height => 15,
      style => {
        'stroke' => 'rgb( 0, 0, 0 )',
        'fill' => "rgb( 255, $color, $color )"
      }
    );
   
    $y += 10;

    ## tissue
    $tissue =~ s/%.*$//;
    $grp1->text(
      x => $express_x + $x + 4,
      y => $express_y + $y + 2,
      -cdata => $tissue,
      style => {
        'font'=> [ qw(Arial) ],
        'font-size' => 13
      }
    );
  }


  #### bar graph --- CHANGE FOLD
  my $tissue_specific = 0;
  foreach my $tissue_aux ( @TISSUE_ORDER_FOLD )
  {
    my $tissue = $tissue_aux;
    my $fold = $SPECIFIC{ $tissue };
    $tissue_specific++;

    my $y = ( $tissue_specific * 20 ) - 20;
    my $x = ( ( $fold * $fold_scale * 0.90 ) / $max_divisor ) * $max_barsize;
    my $x_aux = $express_x;
    $x_aux -= $x if ( $x >= 0 );
    my $x_width = abs( $x );

    $grp1->rectangle(
      x => $x_aux,
      y => $express_y + $y + 3,
      width => $x_width,
      height => 9,
      style => {
        'stroke' => 'rgb( 0, 0, 0 )',
        'fill' => "rgb( 0, 100, 250 )"
      }
    );
  }

  ###### PRINT GRAPH TO FILE
  open FILE, ">$output";
  print FILE fix_svg( $grp1->xmlify );
  close FILE;
}



###################################### FIX SVG!!
sub fix_svg
{
  my $svg_data = shift;

  my ( $width, $height ) = ( 0, 0 );
  $svg_data =~ s/\s(\d+)="(\d+)"//im;
  ( $width, $height ) = ( $1, $2 );

  $svg_data =~ s/width="(.*?)"/width="$width"/im;
  $svg_data =~ s/height="(.*?)"/height="$height"/im;

  return $svg_data;
}



###################################### DRAW GRAPH
sub draw_graph
{
  my $aux_tissue = shift;
  my %TISSUES = %{ $aux_tissue };
  my $output = shift;

  ####### EXPRESS GRAPH
  my $express_x = 0;
  my $express_y = 10;
  my $max_barsize = 350;

  $express_x += 50; ## vspace

  #### calcule median
  my $median = 0;
  my $max_express = 0;
  my $num_tissues = scalar keys %TISSUES;
  
  #init graph
  my $grp1 = SVG->new(
    $max_barsize + $express_x + 140,
    $num_tissues * 20 + $express_y + 70
  );

  my @EXP_MEDIAN = sort { $a <=> $b } values %TISSUES;
  my $exp_median_size = scalar @EXP_MEDIAN;
  if ( $exp_median_size % 2 ) {
    $median = ( $EXP_MEDIAN[ $exp_median_size / 2 - 1 ]
      + $EXP_MEDIAN[ $exp_median_size / 2 ] ) / 2;
  } else {
    $median = $EXP_MEDIAN[ ( $exp_median_size + 1 ) / 2 - 1 ];
  }

  $gene_exp = $median;
  $max_express = $EXP_MEDIAN[-1];

  my $max_divisor = 12;
  if ($max_express > $max_divisor) {
    $max_divisor = $max_express;
  }
  $max_divisor++ if ($max_divisor % 2);

  #### scale FPKM line
  $express_y += 20;
  $grp1->text(
    x => $express_x + $max_barsize + 15,
    y => $express_y + 8,
    -cdata => 'FPKM',
    style => {
      'font'=> [ qw(Arial) ],
      'font-size' => 13
    }
  );

  $grp1->line(
    x1 => $express_x,
    y1 => $express_y,
    x2 => $express_x + $max_barsize,
    y2 => $express_y,
    style => {
      'stroke' => 'rgb( 0, 0, 0 )'
    }
  );

  foreach my $steps ( 0..10 ) {
    unless ( $steps % 5 ) {
      $grp1->line(
        x1 => $express_x + ( $steps * $max_barsize / 10 ),
        y1 => $express_y,
        x2 => $express_x + ( $steps * $max_barsize / 10 ),
        y2 => $express_y + 8,
        style => {
          'stroke' => 'rgb( 0, 0, 0 )'
        }
      );
      
      my $express = int ( ( $max_divisor / 10 ) * $steps );
      my $aux_x = $express_x + ( $steps * $max_barsize / 10 ) + 2;
      my $aux_y = $express_y - 2;
      $grp1->text(
        x => $aux_x,
        y => $aux_y,
        -cdata => $express,
        style => {
          'font'=> [ qw(Arial) ],
          'font-size' => 10
        },
        transform => "rotate( -25, $aux_x, $aux_y )"
      );
    
    } else {
      
      $grp1->line(
        x1 => $express_x + ( $steps * $max_barsize / 10 ),
        y1 => $express_y,
        x2 => $express_x + ( $steps * $max_barsize / 10 ),
        y2 => $express_y + 4,
        style => {
          'stroke' => 'rgb( 0, 0, 0 )'
        }
      );
    }
  }

  ###
  my $mean = 0;
  foreach my $std_tissues ( keys %TISSUES ) {
    $mean += $TISSUES{ $std_tissues };
  }
  $mean /= $num_tissues;

  {
    #$num_tissues
    my $y_medium = ( $num_tissues * 20 ) / 2 + 40;
    my $aux_x = $express_x - 22;
    my $aux_y = $express_y + $y_medium;
    
    $grp1->text(
      x => $aux_x,
      y => $aux_y,
      -cdata => 'Samples',
      style => {
        'font'=> [ qw(Arial) ],
        'font-size' => 11
      },
      transform => "rotate( -90, $aux_x, $aux_y )"
    );
  }


  #order by gene expression
  my %GENE_ORDER_aux = ();
  my @GENE_ORDER = ();
  foreach my $tissue_aux ( keys %TISSUES ) {
    my $expression = $TISSUES{ $tissue_aux };
    push @{ $GENE_ORDER_aux{ $expression } }, $tissue_aux;
  }

  foreach my $exp_aux ( sort { $a <=> $b } keys %GENE_ORDER_aux ) {
    foreach my $tissue_aux (@{ $GENE_ORDER_aux{ $exp_aux } } ) {
      push @GENE_ORDER, $tissue_aux; 
    }
  }


  #STATISTICS REPORT ###############################
  open FILE_tabgen, ">$output.report";

  print FILE_tabgen "        <div class='col-sm-6'>
          <div class='panel panel-default'>
            <div class='panel-heading'><h5>Expression details</h5></div>
              <table class='tablesorter' id='gene_exp'>
                <thead><tr>
                  <th>Sample</th>
                  <th>Total Expression (FPKM)</th>
              </tr></thead>
              <tbody>\n";


  #### bar graph
  $express_y += 20;
  my $tissue_pos = 0;
  
  foreach my $tissue_aux ( @GENE_ORDER )
  {
    my $tissue = $tissue_aux;
    my $expression = $TISSUES{ $tissue };
    $tissue_pos++;

    my $y = ( $tissue_pos * 20 ) - 20;
    my $x = ( $expression / $max_divisor ) * $max_barsize;
    my $color = 170 - int ( ( $expression / $max_divisor )  * 170 );

    $grp1->rectangle(
      x => $express_x,
      y => $express_y + $y + 3,
      width => $x,
      height => 15,
      style => {
        'stroke' => 'rgb( 0, 0, 0 )',
        'fill' => "rgb( 255, $color, $color )"
      }
    );

    $y += 10;
    $tissue =~ s/%.*$//;

    ## tissue
    $grp1->text(
      x => $express_x + $x + 4,
      y => $express_y + $y + 5,
      -cdata=>$tissue,
      style=> {
        'font'=> [ qw(Arial) ],
        'font-size' => 13
      }
    );

    #gen expression table
    print FILE_tabgen "                <tr><td>$tissue</td><td>$expression</td></tr>\n";
  }

  print FILE_tabgen "          </tbody>
        </table>
      </div>
    </div>\n";

  close FILE_tabgen;


  ###### PRINT GRAPH TO FILE
  open FILE, ">$output";
  print FILE fix_svg( $grp1->xmlify );
  close FILE;
}
