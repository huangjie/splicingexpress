#!/usr/bin/perl -w

#The MIT License (MIT)
#
#Copyright (c) 2015 José Eduardo Kroll 
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

use strict;

my %RAW_DATA = ();
my %SAMPLES_aux = ();
my @SAMPLES = ();

## LOAD DATA
open RAW, "<$ARGV[0]";

while ( <RAW> ) {
  chomp;
  my ( $seqname, $gene, $chr, $strand, $type, $exons ) = split /\t/, $_;

  if ( $type eq 'ngs' ) {
    #Line example
    #NM#003672_adipose_1.2404195605  CDC14A  chr1    +       ngs     100818023-100818559
    my ( $sample, $FPKM ) = $seqname =~ /!(\S+?)!(\S+)$/i;
    $seqname =~ s/^(\S+?)!.+$/$1/i;

    $SAMPLES_aux{ $sample } = 0;
    $RAW_DATA{ "$gene:$chr:$strand" }{ "$seqname:$sample:$FPKM" } = $exons;

  } elsif ($type eq 'refseq') {
    $RAW_DATA{ "$gene:$chr:$strand" }{ "$seqname:refseq:0" } = $exons;
  }
}
close RAW;

@SAMPLES = sort keys %SAMPLES_aux;


### PROCESS
foreach my $geneinfo ( keys %RAW_DATA ) {
  my ( $gene, $chr, $strand ) = split /:/, $geneinfo;
  $chr =~ s/chr//i;

  print "GENE\t$gene\nSTRAND\t$strand\nCHRO\t$chr\n";
  
  my %POSMATRIX = ();
  my %POSMATRIX_rev = ();
  my %BINMATRIX = ();

  ### proc posmatrix
  foreach my $seqinfo ( keys %{ $RAW_DATA{ $geneinfo } } ) {
    my ( $seqname, $sample, $FPKM ) = split /:/, $seqinfo;
    my $exons = $RAW_DATA{ $geneinfo }{ $seqinfo };

    ##posmatrix
    my @ALLEXONS = split /\s+/, $exons;
    #### skip seq <= 1 exons
    next if ( scalar @ALLEXONS <= 1 );
    
    foreach my $exon ( @ALLEXONS ) {
      my ( $start, $end ) = split /-/, $exon;
      $POSMATRIX{ $start } = 0;
      $POSMATRIX{ $end } = 0;
    }
  }

  #clean posmatrix
  my @POSMATRIX_aux = sort {$a <=> $b} keys %POSMATRIX;
  %POSMATRIX = ();

  foreach my $posm ( 0 .. $#POSMATRIX_aux ) {
    $POSMATRIX{ $POSMATRIX_aux[ $posm ] } = $posm * 2;
    $POSMATRIX_rev{ $posm * 2 } = $POSMATRIX_aux[ $posm ];
  }

  #binary seqs
  #initialize
  my %EXPRESS_AUX = ();
  foreach my $sample ( @SAMPLES ) {
    $EXPRESS_AUX{ $sample } = 0;
  }

  foreach my $seqinfo ( keys %{ $RAW_DATA{ $geneinfo } } ) {
    my ( $seqname, $sample, $FPKM ) = split /:/, $seqinfo;
    $FPKM =~ s/\(\S+\)//i;  ### remove std err -- only needs after
    $EXPRESS_AUX{ $sample } += $FPKM;
    
    my $exons = $RAW_DATA{ $geneinfo }{ $seqinfo };
    my @ALLEXONS = split /\s+/, $exons;

    ##### skips 1 exon seqs
    next if ( scalar @ALLEXONS <= 1 );
    
    my @SEQBIN = ();
    foreach my $exon ( @ALLEXONS ) {
      my ( $start, $end ) = split /-/, $exon;
      foreach my $pos ( $POSMATRIX{ $start } .. $POSMATRIX{ $end } ) {
        $SEQBIN[$pos] = 1;
      }
    }

    foreach my $seqbinend ( 0 .. $#SEQBIN ) {
      $SEQBIN[ $seqbinend ] = '-' unless ( $SEQBIN[$seqbinend] );
    }

    my $binseq = join '', @SEQBIN;
    $binseq =~ s/^(-+)/' ' x length($1)/ex;
    $binseq =~ s/(-+)$/' ' x length($1)/ex;
    $BINMATRIX{ $seqinfo } = $binseq;

    print "\tSEQS_ALL\t$seqname\t$sample\t$FPKM\t.$binseq\n";
  }

  #print expression per sample
  print "\n";
  foreach my $sample ( keys %EXPRESS_AUX ) {
    my $FPKM = $EXPRESS_AUX{ $sample };
    if ( $sample ne 'refseq' ) {
      print "\tEXPRESS\t$sample\t$FPKM\n";
    }
  }
  print "\n";


  ## Find events
  #AS_SEQS_A       CUFF.63913.2.chr20 prostate 25.6832898609
  my %CLUST_RES = ();

  ### CHECK 'A' SIDE
  foreach my $seqinfoA ( keys %BINMATRIX ) {
    my ( $seqnameA, $sampleA, $FPKMA ) = split /:/, $seqinfoA;
    my $binseqA = $BINMATRIX{ $seqinfoA };

    foreach my $seqinfoB ( keys %BINMATRIX ) {
      next if ( $seqinfoA eq $seqinfoB );

      my ( $seqnameB, $sampleB, $FPKMB ) = split /:/, $seqinfoB;
      my $binseqB = $BINMATRIX{ $seqinfoB };

      ##compare
      my $minsize = length $binseqA;
      my $auxsize = length $binseqB;
      $minsize = $auxsize if ( $minsize > $auxsize );

      my $bin_diff = '';
      foreach my $posbin ( 0 .. $minsize ) {
        my $charA = substr( $binseqA, $posbin, 1 );
        my $charB = substr( $binseqB, $posbin, 1 );

        if ( $charA eq '-' && $charB eq '1' ) {
          $bin_diff .= 'A';
        } elsif ($charA eq '1' && $charB eq '-') {
          $bin_diff .= 'B';
        } elsif ($charA eq $charB) {
          $bin_diff .= $charA;
        } else {
          $bin_diff .= ' ';
        }
      }

      ### find events
      #SKIPPING
      while ( $bin_diff =~ /-(A+)-/cg ) {
        my ( $start, $end ) = ($-[ 1 ], $+[ 1 ] - 1);
        $CLUST_RES{ SKIP }{ "$start:$end" }{ A }{ $seqinfoA } = 0;
        $CLUST_RES{ SKIP }{ "$start:$end" }{ B }{ $seqinfoB } = 0;
      }

      #RETENTION
      while ( $bin_diff =~ /1(A+)1/cg ) {
        my ( $start, $end ) = ( $-[ 1 ] - 1, $+[ 1 ] );
        $CLUST_RES{ RETENT }{ "$start:$end" }{ A }{ $seqinfoA } = 0;
        $CLUST_RES{ RETENT }{ "$start:$end" }{ B }{ $seqinfoB } = 0;
      }

      #ALT5
      my $event = 'ALT5';
      $event = 'ALT3' if ( $strand eq '-' );
      while ( $bin_diff =~ /1+(A+)-/cg ) {
        my ( $start, $end ) = ( $-[ 1 ] - 1, $+[ 1 ] - 1 );
        $CLUST_RES{ $event }{ "$start:$end" }{ A }{ $seqinfoA } = 0;
        $CLUST_RES{ $event }{ "$start:$end" }{ B }{ $seqinfoB } = 0;
      }

      #ALT3
      $event = 'ALT3';
      $event = 'ALT5' if ( $strand eq '-' );
      while ( $bin_diff =~ /-(A+)1/cg ) {
        my ( $start, $end ) = ( $-[1], $+[1] );
        $CLUST_RES{ $event }{ "$start:$end" }{ A }{ $seqinfoA } = 0;
        $CLUST_RES{ $event }{ "$start:$end" }{ B }{ $seqinfoB } = 0;
      }

    }
  }


  ### print event cluster
  foreach my $type ( keys %CLUST_RES ) {
    foreach my $posevent ( keys %{ $CLUST_RES{ $type } } ) {
      my ( $ev_start, $ev_end ) = split /:/, $posevent;
      my $posevent_print = '';

      foreach my $pos ( $ev_start .. $ev_end ) {
        $posevent_print .= "A:$pos#";
      }

      print "ALIGN_CLUST\t$posevent_print\n";

      my %ASEXPRESS = ();
      my %ASESTDERR = ();

      #initialize
      foreach my $side ( 'A', 'B' ) {
        foreach my $sample ( @SAMPLES ) {
          $ASEXPRESS{ $side }{ $sample } = 0;
          $ASESTDERR{ $side }{ $sample } = 0; 
        }
      }

      #AS_EXPRESS_A    ovary   0
      my %SUPPEXIST = ();
      my %SAMSUPPEXIST = ();

      foreach my $side ('A', 'B') {
        foreach my $seqinfo ( keys %{ $CLUST_RES{ $type }{ $posevent }{ $side } } ) {
          my ( $seqname, $sample, $FPKM_aux ) = split /:/, $seqinfo;
          my ($FPKM, $stderr) = $FPKM_aux =~ /(\S+)\((\S+)\)/;

          my $binseq = $BINMATRIX{ $seqinfo };

          if ( $sample eq 'refseq' ) {
            $SUPPEXIST{ $side }{ $seqname } = 0;
          } else {
            print "\tAS_SEQS_$side\t$seqname\t$sample\t$FPKM($stderr)\t.$binseq\n";
            $ASEXPRESS{ $side }{ $sample } += $FPKM;
            $ASESTDERR{ $side }{ $sample } += ( $stderr ** 2 );

            $SAMSUPPEXIST{ $side }++;
          }
        }
      }

      #finalize stderr sum
      foreach my $side ( 'A', 'B' ) {
        foreach my $sample ( @SAMPLES ) {
          if ( $ASESTDERR{ $side }{ $sample } ) {
            $ASESTDERR{ $side }{ $sample } = sqrt( $ASESTDERR{ $side }{ $sample } );
          }
        }
      }

      my $asbinpos = '';
      my $ev_size = 1 + $ev_end - $ev_start;
      $asbinpos = ' ' x $ev_end;
      substr( $asbinpos, $ev_start, $ev_size, 'A' x $ev_size );
      print "\tAS_BINPOS\t.$asbinpos\n";

      #AS_DESC SKIP
      #AS_CLASS        SIMPLE
      #AS_GENPOS       45027336:45027410

      if ( exists $SAMSUPPEXIST{ A } && exists $SAMSUPPEXIST{ B } ) {
        print "\tAS_DESC\t$type\n";
      }
      print "\tAS_CLASS\tSIMPLE\n";

      my $genpos_s = $POSMATRIX_rev{ $ev_start };
      my $genpos_e = $POSMATRIX_rev{ $ev_end };
      print "\tAS_GENPOS\t$genpos_s:$genpos_e\n";

      my ( $suppA, $suppB ) = ( 0, 0 );
      my ( $suppseqsA, $suppseqsB ) = ( '', '' );
      if ( exists $SUPPEXIST{ A } ) {
        my @auxseqs = keys %{ $SUPPEXIST{ A } };
        $suppA = scalar @auxseqs;
        $suppseqsA = join ',', @auxseqs;
      }

      if ( exists $SUPPEXIST{ B } ) {
        my @auxseqs = keys %{ $SUPPEXIST{ B } };
        $suppB = scalar @auxseqs;
        $suppseqsB = join ',', @auxseqs;
      }

      print "\tAS_EXISTS\t$suppA/$suppB\t$suppseqsA/$suppseqsB\n\n";

      #print AS EXPRESS 
      foreach my $side ( 'A', 'B' ) {
        foreach my $sample ( sort keys %{ $ASEXPRESS{ $side } } ) {
          my $FPKM = $ASEXPRESS{ $side }{ $sample };
          my $stderr = $ASESTDERR{ $side }{ $sample };

          if ( $sample ne 'refseq' ) {
            print "\tAS_EXPRESS_$side\t$sample\t$FPKM($stderr)\n";
          }
        }
      }

      print "\n";
    }
  }

  ### gene end
  print "\n" . '--------------------------------------------------------------------------------' . "\n";
}

print "END";
