#!/usr/bin/perl -w

#The MIT License (MIT)
#
#Copyright (c) 2015 José Eduardo Kroll 
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.


use strict;
my $old_seqname = '';
my $old_strand = '';
my $old_chr = '';

my @STARTS = ();
my @BLOCKS = ();

my $tissue = $ARGV[ 1 ];
my $line_numb = 0;
my $filename = $ARGV[ 0 ];

open GTF, "<$filename";
while ( <GTF> ) {
  chomp;
  my $linha = $_;
  my @DATA = split /\t/, $linha;

  #line example
  #chr1    Cufflinks       exon    11874   12227   .       +       .       gene_id "XLOC_000001"; transcript_id "TCONS_00000001";
  my $chr = $DATA[ 0 ];
  $chr = 'chr' . $chr unless ( $chr =~ /^chr/i );

  my $start = $DATA[ 3 ] - 1;
  my $block = $DATA[ 4 ] - $start;
  my $strand = $DATA[ 6 ];
  my $transexon = $DATA[ 2 ];
  next unless ( $transexon =~ /exon/i );

  $line_numb++;

  if ( ! $DATA[ 8 ] =~ /((?:R|F)PKM|transcript_id)/i ) {
    print STDERR "\n\tWarning!!! No FPKM or transcript_id annotation tags in file '$filename', line $line_numb\n";
  }

  ( my $FPKM ) = $DATA[ 8 ] =~ /(?:R|F)PKM\s*"(\S+?)";/i;
  next if ( ! defined $FPKM );
  next if ( $FPKM =~ /[a-z]/i );
  next if ( $FPKM <= 0 );

  ##get FPKM std error
  #conf_lo "0.000000"; conf_hi "0.000000";
  ( my $conf_lo ) = $DATA[ 8 ] =~ /conf_lo\s*"(\S+?)";/i;
  ( my $conf_hi ) = $DATA[ 8 ] =~ /conf_hi\s*"(\S+?)";/i;
  my $stderr = 0;

  if (defined $conf_lo && defined $conf_hi) {
    $stderr = sprintf("%.6f", ($conf_hi - $conf_lo) / (2 * 1.96));
  }

  ( my $seqname ) = $DATA[ 8 ] =~ /transcript_id\s*"(\S+?)";/i;
  $seqname =~ s/_/#/g;
  $seqname .= "!$tissue!$FPKM($stderr)";

  if ( $seqname ne $old_seqname && $old_seqname ) {
    ## print data cache
    my @LINHA = ( '' ) x 21;
    $LINHA[ 9 ] = $old_strand;
    $LINHA[ 10 ] = $old_seqname;
    $LINHA[ 14 ] = $old_chr;
    $LINHA[ 19 ] = join ',', @BLOCKS;
    $LINHA[ 21 ] = join ',', @STARTS;
    print join( "\t", @LINHA ) . "\n";

    ## clean cache
    @STARTS = ();
    @BLOCKS = ();
  }

  push @STARTS, $start;
  push @BLOCKS, $block;

  $old_seqname = $seqname;
  $old_strand = $strand;
  $old_chr = $chr;

  #strand         9
  #seqname        10
  #chr            14
  #blocksize      19
  #tstart         21
}
close GTF;


## print data cache
my @LINHA = ( '' ) x 21;
$LINHA[ 9 ] = $old_strand;
$LINHA[ 10 ] = $old_seqname;
$LINHA[ 14 ] = $old_chr;
$LINHA[ 19 ] = join ',', @BLOCKS;
$LINHA[ 21 ] = join ',', @STARTS;
print join( "\t", @LINHA ) . "\n";
