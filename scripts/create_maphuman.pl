#!/usr/bin/perl -w

#The MIT License (MIT)
#
#Copyright (c) 2015 José Eduardo Kroll 
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.


use strict;

######## INPUT ARGS
my $QUERYCHR = $ARGV[ 0 ];
my $essem_id = $ARGV[ 1 ];

######## GLOBAL VARS;
#$| = 1;
my %SEQBEST = ();
my %GENENAMES = ();
my %SEQTYPE = ();

######## OPEN FILES AND CALL FUNCTIONS
my @input_files = ( "./ref_data/$essem_id\_Ali.txt.gz" );
my @FILESdir = `ls -1 ./tmp/*.psl`;
push @input_files, @FILESdir;
my $refFlat = "./ref_data/$essem_id\_Flat.txt.gz";


foreach my $filename ( @input_files ) {
  my $comando_open = $filename;
  if ( $filename =~ /\.gz/ ) {
    $comando_open = 'gunzip -c ' . $comando_open . ' |';
  }

  open INPUT, $comando_open || die "$filename doesn't exist\n\n";
  while( <INPUT> ) {
    chomp $_; 
    my @v = split( /\t/, $_ );
    shift @v;
   
    my $typeseq = '';
    if ( $filename =~ /Ali\.txt\.gz/) {
      $typeseq = "refseq";
    } elsif ( $filename =~ /gtf_/i ) {
      $typeseq = "ngs";
    } else {
      print "SEQ TYPE ERROR!\n";
      exit;
    }

    if ( get_pid( \@v, $typeseq ) ) {
      ### define seq type
      $SEQTYPE{ $v[ 9 ] } = $typeseq;
    }
  }
  close INPUT;
}


open INPUT, "gunzip -c $refFlat |" || die "$refFlat doesn't exist\n\n";
while ( <INPUT> ) {
  chomp $_;
  my ( $genename, $refseq ) = split /\t/, $_;
  $GENENAMES{ $refseq } = $genename;
}
close INPUT;


####### ORGANIZE DATA INTO HASHES
my %CLUSTERseq = ();
my %CLUSTERchk = ();

foreach my $sequences ( keys %SEQBEST ) {
  my $seqrep_id = 0;

  foreach my $seqrep_id ( 0 .. $#{ $SEQBEST{ $sequences } } )
  {
    my $seqdata = ${ $SEQBEST{ $sequences } }[ $seqrep_id ];
    next unless ( $seqdata );

    my ( $identy, $cover, $alignment ) = split /:/, $seqdata;
    my ( $seqname, $chr, $strand, $posdata ) = split /,/, $alignment;
    my $seqname_old = $seqname;
    $seqname .= ">$seqrep_id";

    my $seqtype = 'non';
    if ( $seqname =~ /^N._/i ) {
      $seqtype = 'ref';
    }

    my $exons_numb = 0;
    my $aux_posdata = '';

    ### check intron size. intron has to have >= 50 bp
    my $posdata_auxdoaux = $posdata;
    my $posdata_check = $posdata;

    while ( $posdata_auxdoaux =~ s/-(\d+)\)#\((\d+)-/-/i ) {
      my ( $endexon1, $startexon2 ) = ( $1, $2 );
      if ( ( $startexon2 - $endexon1 - 1 ) < 50 ) {
        $posdata =~ s/-$endexon1\)#\($startexon2-/-/i;
      }
    }

    if ( $posdata_check ne $posdata ) {
      ${ $SEQBEST{ $sequences } }[ $seqrep_id ] =
        "$identy:$cover:$seqname_old,$chr,$strand,$posdata";
    }

    $posdata =~ s/^\((\d+)-/\(s$1-/i;
    $posdata =~ s/-(\d+)\)#$/-e$1\)#/i;

    while ( $posdata =~ s/\(([0-9se]+?)-([0-9se]+?)\)#//i ) {
      my ( $start, $end ) = ( $1, $2 );
      unless ( $start =~ /(s|e)/i ) {
        $CLUSTERseq{ $seqtype }{ $chr }{ $strand }{ $start }{ $seqname } = 0;
      }

      unless ( $end =~ /(s|e)/i ) {
        $CLUSTERseq{ $seqtype }{ $chr }{ $strand }{ $end }{ $seqname } = 0;
      }

      $aux_posdata .= "$start:$end#";
      $exons_numb++;
    }


    ### record data
    if ( $seqtype eq 'ref' ) {
      push @{$CLUSTERchk{$seqtype}{$chr}{$strand}{$seqname}}, $aux_posdata;

    } elsif ($exons_numb == 1 && $seqtype eq 'non') {
      #### SELECTS SEQ >= 30nt
      $aux_posdata =~ s/(s|e)//gi;
      my ( $start, $end ) = $aux_posdata =~ /^([0-9se]+):([0-9se]+)#$/i;

      if ( 1 + $end - $start >= 30 ) {
        push @{$CLUSTERchk{$seqtype}{$chr}{$strand}{$seqname}}, $aux_posdata;
      }

    }
  }
}

######## CLUSTERIZE SEQS - BY SPLICING BORDERS
my $slippage = 3;
my %CLUSTERscore = ();
foreach my $chr ( keys %{ $CLUSTERseq{ 'ref' } } ) {
  foreach my $strand ( keys %{ $CLUSTERseq{ 'ref' }{ $chr } } ) {
    foreach my $position (keys %{ $CLUSTERseq{ 'ref' }{ $chr }{ $strand } } ) {
      foreach my $refseq (keys %{ $CLUSTERseq{ 'ref' }{ $chr }{ $strand }{ $position } } ) {
        foreach my $posearch ( ( $position - $slippage ) .. ( $position + $slippage ) ) {
          foreach my $anystrand ( '-', '+' ) {
            foreach my $sameborderseq ( keys %{ $CLUSTERseq{ 'non' }{ $chr }{ $anystrand }{ $posearch } } ) {
              $CLUSTERscore{ $sameborderseq }{ $refseq }++; 
            }
          }
        }
      }
    }
  }
}


######## clusterize seqs without intron - 
foreach my $chr ( keys %{ $CLUSTERchk{ 'ref' } } ) {
  foreach my $strand ( keys %{ $CLUSTERchk{ 'ref' }{ $chr } }) {
    foreach my $refseq ( keys %{ $CLUSTERchk{ 'ref' }{ $chr }{ $strand } } ) {
      foreach my $aux_posdata_ref (@{ $CLUSTERchk{ 'ref' }{ $chr }{ $strand }{ $refseq } } ) {
        $aux_posdata_ref =~ s/(s|e)//gi;

        foreach my $anystrand ( '-', '+' ) {
          foreach my $seqname ( keys %{ $CLUSTERchk{ 'non' }{ $chr }{ $anystrand } }) {
            foreach my $aux_posdata_non ( @{ $CLUSTERchk{ 'non' }{ $chr }{ $anystrand }{ $seqname } }) {
              $aux_posdata_non =~ s/(s|e)//gi;
              my $total_cover = 0;

              ## compar
              foreach my $refexons ( split /#/, $aux_posdata_ref ) {
                if ( $refexons ) {
                  my ( $refstart, $refend ) = split /:/, $refexons;
                  foreach my $nonexons ( split /#/, $aux_posdata_non ) {
                    if ( $nonexons ) {
                      my ( $nonstart, $nonend ) = split /:/, $nonexons;

                      if ( $nonstart < $refend && $nonend > $refstart ) {
                        if ( $nonend > $refend && $nonstart > $refstart ) {
                          $total_cover += $refend - $nonstart;
                        } elsif ( $refend > $nonend && $refstart > $nonstart ) {
                          $total_cover += $nonend - $refstart;
                        } elsif ( $refend > $nonend && $refstart < $nonstart ) {
                          $total_cover += $nonend - $nonstart;
                        } elsif ( $refend < $nonend && $refstart > $nonstart ) {
                          $total_cover += $refend - $refstart;
                        }
                      }
                    }
                  }
                }
              }

              if ( $total_cover >= 30 ) {
                if ( exists $CLUSTERscore{ $seqname }{ $refseq } ) {
                  if ( $total_cover > $CLUSTERscore{ $seqname }{ $refseq } ) {
                    $CLUSTERscore{ $seqname }{ $refseq } = $total_cover;
                  }
                } else {
                  $CLUSTERscore{ $seqname }{ $refseq } = $total_cover;
                }
              }
            
            }
          }
        }
      }
    }
  }
}


### select best cluster
open OUTPUT, ">./tmp/mapping_$QUERYCHR.txt";

##### print refseqs
foreach my $sequences ( keys %SEQBEST ) {
  if ( $sequences =~ /^N._/i ) {
    foreach my $seqdata (@{ $SEQBEST{ $sequences } } ) {
      next unless ( $seqdata );

      my ( $identy, $cover, $alignment ) = split /:/, $seqdata;
      my ( $seqname, $chr, $strand, $posdata ) = split /,/, $alignment;
      $posdata =~ s/(\)|\()//gi;
      $posdata =~ s/#/ /gi;

      my $gene = $GENENAMES{ $seqname };
      print OUTPUT "$sequences\t$gene\t$chr\t$strand\t$SEQTYPE{$seqname}\t$posdata\n";
    }
  }
}


###### print non refseqs
foreach my $sameborderseq ( keys %CLUSTERscore ) {
  my $bestrefseq = '';
  my %score_aux = ();

  foreach my $refseq (keys %{ $CLUSTERscore{ $sameborderseq } } ) {
    my $score = $CLUSTERscore{ $sameborderseq }{ $refseq };
    $score_aux{ $score } .= "$refseq,";
  }

  my ( $score ) = sort { $b <=> $a } keys %score_aux;
  my $refseqlist = $score_aux{ $score };

  my %genelist_aux = ();
  foreach my $eachrefseq ( split /,/, $refseqlist ) {
    $eachrefseq =~ s/^(.+)>\d+$/$1/i;
    $genelist_aux{ $GENENAMES{ $eachrefseq } } = 0;
  }

  my ( $repseq_id ) = $sameborderseq =~ />(\d+)$/i;
  $sameborderseq =~ s/^(.+)>\d+$/$1/i;

  my $seqdata = ${ $SEQBEST{ $sameborderseq }}[ $repseq_id - 1 ];
  my ( $identy, $cover, $alignment, $seqname, $chr, $strand, $posdata ) = 
    ( '', '', '', '', '', '', '' );

  if ( $seqdata ) {
    ( $identy, $cover, $alignment ) = split /:/, $seqdata;
    ( $seqname, $chr, $strand, $posdata ) = split /,/, $alignment;
  } else {
    next;
  }

  $posdata =~ s/(\)|\()//gi;
  $posdata =~ s/#/ /gi;

  foreach my $gene (keys %genelist_aux) {
    print OUTPUT "$sameborderseq\t$gene\t$chr\t$strand\t$SEQTYPE{$sameborderseq}\t$posdata\n";
  }
}
close OUTPUT;



######## FUNCTION TO FILTER AND SELECT BEST SEQS
sub get_pid { 
  my $lineref = shift;
  my @line = @{ $lineref };
  my $typeseq = shift;

  my $chr = $line[ 13 ];
  return 0 unless ( lc $chr eq lc $QUERYCHR );

  my ( $identity, $coverage ) = ( 0, 0 );
  unless ($typeseq eq 'ngs') {
    ( $identity, $coverage ) = pslCalcMilliBad( @line );
  }

  my $seqname = $line[ 9 ];
  my $strand = $line[ 8 ];

  if ( $identity >= 95 && $coverage >= 90 || $typeseq eq 'ngs' ) {
    #seqname        9
    #chr            13
    #strand         8
    #blocksize      18
    #tstart         20

    my @blocksize = split /,/, $line[ 18 ];
    my @tstart = split /,/, $line[ 20 ];
    my $seqdata = "$seqname,$chr,$strand,";

    foreach my $block_num ( 0 .. $#blocksize ) {
      my $start = $tstart[ $block_num ] + 1;
      my $end = $tstart[ $block_num ] + $blocksize[ $block_num ];

      $seqdata .= "($start-$end)#";
    }

    if ( exists $SEQBEST{ $seqname } ) {
      #### ADD DATA TO ARRAY
      foreach my $oldata_num ( 0 .. $#{ $SEQBEST{ $seqname } } ) {
        my $oldata = ${ $SEQBEST{ $seqname } }[ $oldata_num ];
        next unless ( $oldata );

        my ( $identity_aux, $coverage_aux ) = split /:/, $oldata;

        ## USE ONLY BEST ALIGNS
        if ( $coverage > $coverage_aux && $identity > $identity_aux ) {
          ${ $SEQBEST{ $seqname } }[ $oldata_num ] = "$identity:$coverage:$seqdata";
        } elsif ( $coverage == $coverage_aux && $identity == $identity_aux ) {
          push @{ $SEQBEST{ $seqname } }, "$identity:$coverage:$seqdata";
        }
      }

      #### VERIFY REPETITION
      foreach my $oldata_num1 ( 0 .. $#{$SEQBEST{$seqname}} - 1) {
        foreach my $oldata_num2 ( ( $oldata_num1 + 1 ) .. $#{ $SEQBEST{ $seqname } } ) {
          my $oldata1 = ${ $SEQBEST{ $seqname } }[ $oldata_num1 ];
          my $oldata2 = ${ $SEQBEST{ $seqname } }[ $oldata_num2 ];

          if ( $oldata1 && $oldata2 ) {
            if ( $oldata1 eq $oldata2 ) {
              ${ $SEQBEST{ $seqname } }[ $oldata_num2 ] = '';
            }
          }
        }
      }

      #### REMOVE EMPTY INSTANCES
      my @SEQBEST_aux = ();
      foreach my $oldata (@{ $SEQBEST{ $seqname } } ) {
        push @SEQBEST_aux, $oldata if ( $oldata );
      }
      @{ $SEQBEST{ $seqname } } = @SEQBEST_aux;

    } else {
      #### NEW DATA
      push @{ $SEQBEST{ $seqname } }, "$identity:$coverage:$seqdata";
    }
  }

  return 1;
}


####### CALC IDENTITY
sub pslCalcMilliBad { 
  my @cols = @_; 
  # cols[0]  matches 
  # cols[1]  misMatches 
  # cols[2]  repMaches 
  # cols[4]  qNumInsert 
  # cols[6]  tNumInsert 
  # cols[11] qStart 
  # cols[12] qEnd 
  # cols[15] tStart 
  # cols[16] tEnd 

  my $qAliCover = 100.0 * ( $cols[ 12 ] - $cols[ 11 ]) / $cols[ 10 ];

  my $qAliSize = $cols[ 12 ] - $cols[ 11 ]; 
  my $tAliSize = $cols[ 16 ] - $cols[ 15 ]; 

  my $aliSize; 
  $qAliSize < $tAliSize ? $aliSize = $qAliSize : $aliSize = $tAliSize; 
  return 0 if ( $aliSize <= 0 ); 

  my $sizeDiff = $qAliSize - $tAliSize; 
  if ( $sizeDiff < 0 ) {
    $sizeDiff = 0;
  } 

  my $insertFactor = $cols[ 4 ]; 
  my $milliBad = 100 - ( ( 1000 * ( $cols[ 1 ] + $insertFactor + 3 * log( 1 + $sizeDiff ) ) )
    / ( $cols[ 0 ] + $cols[ 2 ] + $cols[ 1 ] ) ) * 0.1;

  return $milliBad, $qAliCover;
}

