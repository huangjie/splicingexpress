#Panda
ailMel1
#---

#American alligator
allMis1
#---

#Lizard
anoCar1
anoCar2
#---

#A. gambiae
anoGam1
#---

#A. mellifera
apiMel1
apiMel2
#---

#Sea hare
aplCal1
#---

#Minke whale
balAcu1
#---

#Cow
bosTau1
bosTau2
bosTau3
bosTau4
bosTau6
bosTau7
bosTau8
#---

#Lancelet
braFlo1
#---

#C. japonica
caeJap1
#---

#C. brenneri
caePb1
caePb2
#---

#C. remanei
caeRem2
caeRem3
#---

#Marmoset
calJac1
calJac3
#---

#Elephant shark
calMil1
#---

#Dog
canFam1
canFam2
canFam3
#---

#Guinea pig
cavPor2
cavPor3
#---

#C. briggsae
cb3
#---

#C. elegans
ce10
ce2
ce4
ce6
#---

#White rhinoceros
cerSim1
#---

#Sloth
choHof1
#---

#Painted turtle
chrPic1
#---

#C. intestinalis
ci1
ci2
#---

#Chinese hamster
criGri1
#---

#Zebrafish
danRer3
danRer4
danRer5
danRer6
danRer7
#---

#Armadillo
dasNov3
#---

#Kangaroo rat
dipOrd1
#---

#D. melanogaster
dm1
dm2
dm3
dm6
#---

#D. pseudoobscura
dp2
dp3
#---

#D. ananassae
droAna1
droAna2
#---

#D. erecta
droEre1
#---

#D. grimshawi
droGri1
#---

#D. mojavensis
droMoj1
droMoj2
#---

#D. persimilis
droPer1
#---

#D. sechellia
droSec1
#---

#D. simulans
droSim1
#---

#D. virilis
droVir1
droVir2
#---

#D. yakuba
droYak1
droYak2
#---

#Tenrec
echTel1
echTel2
#---

#Horse
equCab1
equCab2
#---

#Hedgehog
eriEur1
eriEur2
#---

#Cat
felCat3
felCat4
felCat5
#---

#Fugu
fr1
fr2
fr3
#---

#Atlantic cod
gadMor1
#---

#Chicken
galGal2
galGal3
galGal4
#---

#Stickleback
gasAcu1
#---

#Medium ground finch
geoFor1
#---

#Gorilla
gorGor3
#---

#Naked mole-rat
hetGla1
hetGla2
#---

#Human
hg16
hg17
hg18
hg19
hg38
#---

#Coelacanth
latCha1
#---

#Elephant
loxAfr3
#---

#Wallaby
macEug2
#---

#Turkey
melGal1
#---

#Budgerigar
melUnd1
#---

#Mouse lemur
micMur1
#---

#Mouse
mm7
mm8
mm9
mm10
#---

#Opossum
monDom1
monDom4
monDom5
#---

#Ferret
musFur1
#---

#Microbat
myoLuc2
#---

#Gibbon
nomLeu1
nomLeu2
nomLeu3
#---

#Pika
ochPri2
ochPri3
#---

#Nile tilapia
oreNil2
#---

#Platypus
ornAna1
#---

#Rabbit
oryCun2
#---

#Medaka
oryLat2
#---

#Bushbaby
otoGar3
#---

#Sheep
oviAri1
oviAri3
#---

#Chimp
panTro1
panTro2
panTro3
panTro4
#---

#Baboon
papAnu2
papHam1
#---

#Lamprey
petMar1
petMar2
#---

#Orangutan
ponAbe2
#---

#P. pacificus
priPac1
#---

#Rock hyrax
proCap1
#---

#Megabat
pteVam1
#---

#Rhesus
rheMac2
rheMac3
#---

#Rat
rn4
rn5
rn6
#---

#S. cerevisiae
sacCer1
sacCer2
sacCer3
#---

#Squirrel monkey
saiBol1
#---

#Tasmanian devil
sarHar1
#---

#Shrew
sorAra1
sorAra2
#---

#Squirrel
speTri2
#---

#S. purpuratus
strPur1
strPur2
#---

#Pig
susScr2
susScr3
#---

#Zebra finch
taeGut1
taeGut2
#---

#Tarsier
tarSyr1
#---

#Tetraodon
tetNig1
tetNig2
#---

#Manatee
triMan1
#---

#Tree shrew
tupBel1
#---

#Dolphin
turTru2
#---

#Alpaca
vicPac1
vicPac2
#---

#X. tropicalis
xenTro1
xenTro2
xenTro3
#---
